import io.restassured.http.ContentType
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.Matchers.*
import twitter.Application
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test

class ApplicationTest {
    private var app: Application = Application()

    @BeforeTest fun setUp() {
        app.start(7000)
    }

    @AfterTest fun tearDown() {
        app.stop()
    }

    @Test fun `a user can register with his real name and nickname`() {
        Given {
            port(7000)
            param("fullName", "Jack Bauer")
            param("nickname", "@jack")
            param("password", "my-super-secret-password")
        } When {
            post("/users")
        } Then {
            statusCode(200)
            contentType(ContentType.JSON)
            body("fullName", equalTo("Jack Bauer"))
            body("nickname", equalTo("@jack"))
        }
    }
}