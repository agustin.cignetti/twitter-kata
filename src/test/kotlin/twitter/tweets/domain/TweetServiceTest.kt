package twitter.tweets.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import twitter.common.domain.UserRepository
import twitter.tweets.domain.errors.UserDoesNotExistError
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class TweetServiceTest {
    private val NICKNAME = "@jack"
    private val TWEET = "This is my tweet"

    private lateinit var tweetService: TweetService
    private lateinit var tweetRepository: TweetRepository
    private lateinit var userRepository: UserRepository

    @BeforeTest fun initialize() {
        tweetRepository = mock()
        userRepository = mock()

        tweetService = TweetService(tweetRepository, userRepository)
    }

    @Test fun `an existing user can tweet`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)

        tweetService.tweet(TWEET, NICKNAME)

        verify(tweetRepository).add(TWEET, NICKNAME)
    }

    @Test fun `a non existent user raises an exception`(){
        whenever(userRepository.exists(NICKNAME)).thenReturn(false)

        assertFailsWith<UserDoesNotExistError> { tweetService.tweet(TWEET, NICKNAME) }
    }

    @Test fun `can list a user tweets`() {
        whenever(tweetRepository.retrieve(NICKNAME)).thenReturn(emptyList())

        val result = tweetService.retrieveTweets(NICKNAME)

        assertEquals(emptyList(), result)
    }
}