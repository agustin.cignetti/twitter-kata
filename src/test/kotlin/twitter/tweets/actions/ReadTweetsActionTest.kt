package twitter.tweets.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import twitter.tweets.domain.TweetServiceDefinition
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class ReadTweetsActionTest {
    private val NICKNAME = "@jack"

    private lateinit var readTweetsAction: ReadTweetsAction
    private lateinit var tweetService: TweetServiceDefinition

    @BeforeTest fun initialize() {
        tweetService = mock()
        readTweetsAction = ReadTweetsAction(tweetService)
    }

    @Test fun `a user with no tweets returns an empty list`() {
        whenever(tweetService.retrieveTweets(NICKNAME)).thenReturn(emptyList())

        val result = readTweetsAction.execute(NICKNAME)

        assertEquals(emptyList(), result)
    }
}