package twitter.tweets.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import twitter.tweets.domain.TweetServiceDefinition
import twitter.tweets.domain.errors.UserDoesNotExistError
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class CreateTweetActionTest {
    private val TWEET = "This is my tweet"
    private val NICKNAME = "@jack"
    private val NON_EXISTENT_NICKNAME = "@iDoNotExist"

    private lateinit var createTweetAction: CreateTweetAction
    private lateinit var tweetService: TweetServiceDefinition

    @BeforeTest fun initialize() {
        tweetService = mock()
        createTweetAction = CreateTweetAction(tweetService)
    }

    @Test fun `a user can tweet`() {
        createTweetAction.execute(TWEET, NICKNAME)

        verify(tweetService).tweet(TWEET, NICKNAME)
    }

    @Test fun `a non existent user does not cause an exception`() {
        whenever(tweetService.tweet(TWEET, NON_EXISTENT_NICKNAME)).then { throw UserDoesNotExistError() }

        assertEquals(Unit, createTweetAction.execute(TWEET, NON_EXISTENT_NICKNAME))
    }
}