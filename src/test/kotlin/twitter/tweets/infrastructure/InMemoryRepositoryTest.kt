package twitter.tweets.infrastructure

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class InMemoryRepositoryTest {
    private val TWEET = "This is my tweet"
    private val NICKNAME = "@jack"

    private lateinit var tweetRepository: InMemoryRepository

    @BeforeTest fun initialize() {
        tweetRepository = InMemoryRepository()
    }

    @Test fun `can add a tweet to the repository`() {
        assertEquals(Unit, tweetRepository.add(TWEET, NICKNAME))
    }

    @Test fun `can list tweets from a user`() {
        tweetRepository.add(TWEET, NICKNAME)

        val result = tweetRepository.retrieve(NICKNAME)

        assertEquals(1, result.size)
        assertEquals(TWEET, result[0])
    }
}