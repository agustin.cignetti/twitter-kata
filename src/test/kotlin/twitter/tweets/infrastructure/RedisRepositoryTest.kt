package twitter.tweets.infrastructure

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class RedisRepositoryTest {
    private val TWEET = "This is my tweet"
    private val NICKNAME = "@jack"

    private lateinit var redisRepository: RedisRepository

    @BeforeTest fun initialize() {
        redisRepository = RedisRepository()
    }

    @Test fun `can add a tweet to the repository`() {
        assertEquals(Unit, redisRepository.add(TWEET, NICKNAME))
    }

    @Test fun `can list tweets from a user`() {
        redisRepository.add(TWEET, NICKNAME)

        val result = redisRepository.retrieve(NICKNAME)

        assertNotEquals(0, result.size)
        assertEquals(TWEET, result[0])
    }
}