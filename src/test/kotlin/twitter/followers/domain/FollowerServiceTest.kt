package twitter.followers.domain

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import twitter.followers.domain.errors.UserDoesNotExistError
import twitter.common.domain.UserRepository
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class FollowerServiceTest {
    private val NICKNAME: String = "@jack"
    private val NICKNAME_TO_FOLLOW: String = "@alice"

    private lateinit var followerRepository: FollowerRepository
    private lateinit var followerService: FollowerService
    private lateinit var userRepository: UserRepository

    @BeforeTest fun initialize() {
        followerRepository = mock()
        userRepository = mock()
        followerService = FollowerService(followerRepository, userRepository)
    }

    @Test fun `a user can follow another user from his nickname`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)
        whenever(userRepository.exists(NICKNAME_TO_FOLLOW)).thenReturn(true)

        followerService.followNickname(NICKNAME, NICKNAME_TO_FOLLOW)

        verify(followerRepository).addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)
    }

    @Test fun `anyone can get the list of followers of a user`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)
        whenever(followerRepository.retrieveFollowers(NICKNAME)).thenReturn(emptyList())

        val result = followerService.retrieveFollowers(NICKNAME)

        assertEquals(0, result.size)
    }

    @Test fun `a non existent user fails to create a follower`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)
        whenever(userRepository.exists(NICKNAME_TO_FOLLOW)).thenReturn(false)

        assertFailsWith<UserDoesNotExistError> { followerService.followNickname(NICKNAME, NICKNAME_TO_FOLLOW) }
    }

    @Test fun `when adding a follower, the user is added to the following as well`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)
        whenever(userRepository.exists(NICKNAME_TO_FOLLOW)).thenReturn(true)

        followerService.followNickname(NICKNAME, NICKNAME_TO_FOLLOW)

        verify(followerRepository).addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)
        verify(followerRepository).addFollowing(NICKNAME, NICKNAME_TO_FOLLOW)
    }

    @Test fun `anyone can get the list of whe the user is following`() {
        whenever(followerRepository.retrieveFollowing(NICKNAME)).thenReturn(emptyList())

        val result = followerService.retrieveFollowing(NICKNAME)

        assertEquals(0, result.size)
    }
}