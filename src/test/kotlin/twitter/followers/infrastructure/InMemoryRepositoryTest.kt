package twitter.followers.infrastructure

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class InMemoryRepositoryTest {
    private val NICKNAME_TO_FOLLOW: String = "@alice"
    private val NICKNAME: String = "@jack"

    private lateinit var inMemoryRepository: InMemoryRepository

    @BeforeTest fun initialize() {
        inMemoryRepository = InMemoryRepository()
    }

    @Test fun `can add a follower to the repository`() {
        inMemoryRepository.addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)
    }

    @Test fun `return a list for an existing user`() {
        inMemoryRepository.addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)

        val result = inMemoryRepository.retrieveFollowers(NICKNAME_TO_FOLLOW)

        assertNotEquals(0, result.size)
    }

    @Test fun `return an empty list for a non existent user`() {
        val result = inMemoryRepository.retrieveFollowers(NICKNAME)

        assertEquals(0, result.size)
    }

    @Test fun `return an empty list of following for a user`() {
        val result = inMemoryRepository.retrieveFollowing(NICKNAME)

        assertEquals(0, result.size)
    }

    @Test fun `return a list of users someone is following`() {
        inMemoryRepository.addFollowing(NICKNAME, NICKNAME_TO_FOLLOW)

        val result = inMemoryRepository.retrieveFollowing(NICKNAME)

        assertNotEquals(0, result.size)
    }
}