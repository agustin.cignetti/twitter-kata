package twitter.followers.infrastructure

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

class RedisRepositoryTest {
    private val NICKNAME_TO_FOLLOW: String = "@alice"
    private val NICKNAME: String = "@jack"

    private lateinit var redisRepository: RedisRepository

    @BeforeTest fun initialize() {
        redisRepository = RedisRepository()
    }

    @Test fun `can add a follower to the repository`() {
        redisRepository.addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)
    }

    @Test fun `return a list for an existing user`() {
        redisRepository.addFollowerTo(NICKNAME_TO_FOLLOW, NICKNAME)

        val result = redisRepository.retrieveFollowers(NICKNAME_TO_FOLLOW)

        assertNotEquals(0, result.size)
    }

    @Test fun `return an empty list for a non existent user`() {
        val result = redisRepository.retrieveFollowers(NICKNAME)

        assertEquals(0, result.size)
    }

    @Test fun `return an empty list of following for a user`() {
        val result = redisRepository.retrieveFollowing(NICKNAME_TO_FOLLOW)

        assertEquals(0, result.size)
    }

    @Test fun `return a list of users someone is following`() {
        redisRepository.addFollowing(NICKNAME, NICKNAME_TO_FOLLOW)

        val result = redisRepository.retrieveFollowing(NICKNAME)

        assertNotEquals(0, result.size)
    }
}