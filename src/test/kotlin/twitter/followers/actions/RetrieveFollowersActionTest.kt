package twitter.followers.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import twitter.followers.domain.FollowerServiceDefinition
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class RetrieveFollowersActionTest {
    private val NICKNAME: String = "@jack"

    private lateinit var followerService: FollowerServiceDefinition
    private lateinit var retrieveFollowersAction: RetrieveFollowersAction

    @BeforeTest fun initialize() {
        followerService = mock()
        retrieveFollowersAction = RetrieveFollowersAction(followerService)
    }

    @Test fun `user without followers receives empty list as response`() {
        whenever(followerService.retrieveFollowers(NICKNAME)).thenReturn(emptyList())

        val result = retrieveFollowersAction.execute(NICKNAME)

        assertEquals(0, result.size)
    }
}