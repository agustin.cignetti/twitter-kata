package twitter.followers.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import twitter.followers.domain.FollowerServiceDefinition
import twitter.followers.domain.errors.UserDoesNotExistError
import kotlin.test.BeforeTest
import kotlin.test.Test

class FollowUserActionTest {
    private val NICKNAME_TO_FOLLOW: String = "@alice"
    private val NICKNAME: String = "@jack"

    private lateinit var followUserAction: FollowUserAction
    private lateinit var followerService: FollowerServiceDefinition

    @BeforeTest fun initialize() {
        followerService = mock()
        followUserAction = FollowUserAction(followerService)
    }

    @Test fun `a user can follow another user`() {
        followUserAction.execute(NICKNAME, NICKNAME_TO_FOLLOW)

        verify(followerService).followNickname(NICKNAME, NICKNAME_TO_FOLLOW)
    }

    @Test fun `a user that do not exist on the system cannot be followed`() {
        whenever(followerService.followNickname(NICKNAME, NICKNAME_TO_FOLLOW)).then { throw UserDoesNotExistError() }

        followUserAction.execute(NICKNAME, NICKNAME_TO_FOLLOW)
    }
}