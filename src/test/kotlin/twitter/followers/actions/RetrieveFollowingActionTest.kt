package twitter.followers.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import twitter.followers.domain.FollowerServiceDefinition
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class RetrieveFollowingActionTest {
    private val NICKNAME = "@jack"

    private lateinit var retrieveFollowingAction: RetrieveFollowingAction
    private lateinit var followerService: FollowerServiceDefinition

    @BeforeTest fun initialize() {
        followerService = mock()
        retrieveFollowingAction = RetrieveFollowingAction(followerService)
    }

    @Test fun `user can list who he is following`() {
        whenever(followerService.retrieveFollowing(NICKNAME)).thenReturn(emptyList())

        val result = retrieveFollowingAction.execute(NICKNAME)

        assertEquals(emptyList(), result)
    }
}