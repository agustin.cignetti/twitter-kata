package twitter.users.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Test
import twitter.users.domain.errors.NickNameAlreadyTakenError
import twitter.users.domain.models.User
import twitter.users.domain.models.UserCredentials
import twitter.users.domain.UserServiceDefinition
import kotlin.test.BeforeTest
import kotlin.test.assertEquals
import kotlin.test.assertNull

class CreateUserActionTest {
    private val PASSWORD: String = "my-super-secret-password"
    private val NICKNAME: String = "@Jack"
    private val FULLNAME: String = "Jack Bauer"
    private val USER_CREDENTIALS = UserCredentials(NICKNAME, FULLNAME, PASSWORD)
    private val USER = User(NICKNAME, FULLNAME, PASSWORD)

    private lateinit var userRepository: UserServiceDefinition
    private lateinit var userApi: CreateUserAction

    @BeforeTest fun initialize() {
        userRepository = mock()
        userApi = CreateUserAction(userRepository)
    }

    @Test fun `create a new user`() {
        userApi.execute(NICKNAME, FULLNAME, PASSWORD)

        verify(userRepository).createUser(USER_CREDENTIALS)
    }

    @Test fun `return User container, containing user information`() {
        whenever(userRepository.createUser(USER_CREDENTIALS)).thenReturn(USER)

        val result = userApi.execute(NICKNAME, FULLNAME, PASSWORD)

        assertEquals(USER, result)
    }

    @Test fun `returns an empty response if the nickname is already taken`() {
        whenever(userRepository.createUser(USER_CREDENTIALS)).then { throw NickNameAlreadyTakenError() }

        val result = userApi.execute(NICKNAME, FULLNAME, PASSWORD)

        assertNull(result)
    }
}