package twitter.users.actions

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import twitter.users.domain.UserServiceDefinition
import twitter.users.domain.models.UserCredentialsUpdate
import kotlin.test.BeforeTest

class UpdateUserActionTest {
    private val NICKNAME: String = "@Jack"
    private val FULLNAME: String = "Jack Bauer"
    private val USER_CREDENTIALS = UserCredentialsUpdate(NICKNAME, FULLNAME)

    private lateinit var userRepository: UserServiceDefinition
    private lateinit var action: UpdateUserAction

    @BeforeTest fun initialize() {
        userRepository = mock()
        action = UpdateUserAction(userRepository)
    }

    @Test fun `update an existing user's fullName works`() {
        action.execute(NICKNAME, FULLNAME)

        verify(userRepository).updateUser(USER_CREDENTIALS)
    }
}