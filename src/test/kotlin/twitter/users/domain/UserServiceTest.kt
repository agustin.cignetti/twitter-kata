import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import twitter.common.domain.UserRepository
import twitter.users.domain.*
import twitter.users.domain.errors.NickNameAlreadyTakenError
import twitter.users.domain.models.User
import twitter.users.domain.models.UserCredentials
import twitter.users.domain.models.UserCredentialsUpdate
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserServiceTest {

    private val PASSWORD: String = "my-super-secret-password"
    private val NICKNAME: String = "@Jack"
    private val FULLNAME: String = "Jack Bauer"
    private val UPDATED_FULLNAME: String = "Mr. Jack Bauer"
    private val USER_CREDENTIALS = UserCredentials(NICKNAME, FULLNAME, PASSWORD)
    private val USER = User(NICKNAME, FULLNAME, PASSWORD)
    private val UPDATED_CREDENTIALS = UserCredentialsUpdate(NICKNAME, UPDATED_FULLNAME)
    private val UPDATED_USER = User(NICKNAME, UPDATED_FULLNAME, PASSWORD)

    private lateinit var userService: UserService
    private lateinit var userRepository: UserRepository

    @BeforeTest fun initialize() {
        userRepository = mock()
        userService = UserService(userRepository)
    }

    @Test fun `create a user adds a user to a store`() {
        val result = userService.createUser(USER_CREDENTIALS)

        verify(userRepository).add(USER)
        assertEquals(USER, result)
    }

    @Test fun `duplicate user raises an exception`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)

        assertFailsWith<NickNameAlreadyTakenError> { userService.createUser(USER_CREDENTIALS) }
    }

    @Test fun `user can update his credentials`() {
        whenever(userRepository.exists(NICKNAME)).thenReturn(true)
        whenever(userRepository.retrieve(NICKNAME)).thenReturn(USER)

        val result = userService.updateUser(UPDATED_CREDENTIALS)

        verify(userRepository).update(UPDATED_USER)
        assertEquals(UPDATED_USER, result)
    }
}