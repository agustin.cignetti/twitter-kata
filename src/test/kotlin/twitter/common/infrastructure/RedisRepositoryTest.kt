package twitter.common.infrastructure

import twitter.users.domain.models.User
import twitter.common.infrastructure.RedisRepository
import kotlin.test.*

class RedisRepositoryTest {
    private val PASSWORD: String = "my-super-secret-password"
    private val NICKNAME: String = "@Jack"
    private val NON_EXISTENT_NICKNAME: String = "@iDoNotExist"
    private val FULLNAME: String = "Jack Bauer"
    private val UPDATED_FULLNAME: String = "Mr. Jack Bauer"
    private val USER = User(NICKNAME, FULLNAME, PASSWORD)
    private val UPDATED_USER = User(NICKNAME, UPDATED_FULLNAME, PASSWORD)

    private lateinit var redisRepository: RedisRepository

    @BeforeTest fun initialize() {
        redisRepository = RedisRepository()
    }

    @Test fun `new user does not fail when checking duplicate`() {
        assertFalse(redisRepository.exists(NON_EXISTENT_NICKNAME))
    }

    @Test fun `existing user fails when checking duplicate`() {
        redisRepository.add(USER)

        assertTrue(redisRepository.exists(NICKNAME))
    }

    @Test fun `user can be retrieved`() {
        redisRepository.add(USER)

        val result = redisRepository.retrieve(NICKNAME)

        assertEquals(USER, result)
    }

    @Test fun `user can be updated`() {
        redisRepository.add(USER)

        redisRepository.update(UPDATED_USER)
        val result = redisRepository.retrieve(NICKNAME)

        assertEquals(UPDATED_USER, result)
    }
}