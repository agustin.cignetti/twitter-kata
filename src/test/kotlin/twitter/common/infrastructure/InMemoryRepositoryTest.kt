package twitter.common.infrastructure

import twitter.users.domain.models.User
import kotlin.test.*

class InMemoryRepositoryTest {
    private val PASSWORD: String = "my-super-secret-password"
    private val NICKNAME: String = "@Jack"
    private val FULLNAME: String = "Jack Bauer"
    private val UPDATED_FULLNAME: String = "Mr. Jack Bauer"
    private val USER = User(NICKNAME, FULLNAME, PASSWORD)
    private val UPDATED_USER = User(NICKNAME, UPDATED_FULLNAME, PASSWORD)

    private lateinit var inMemoryRepository: InMemoryRepository

    @BeforeTest
    fun initialize() {
        inMemoryRepository = InMemoryRepository()
    }

    @Test
    fun `new user does not fail when checking duplicate`() {
        assertFalse(inMemoryRepository.exists(NICKNAME))
    }

    @Test
    fun `existing user fails when checking duplicate`() {
        inMemoryRepository.add(USER)

        assertTrue(inMemoryRepository.exists(NICKNAME))
    }

    @Test
    fun `user can be retrieved`() {
        inMemoryRepository.add(USER)

        val result = inMemoryRepository.retrieve(NICKNAME)

        assertEquals(USER, result)
    }

    @Test
    fun `user can be updated`() {
        inMemoryRepository.add(USER)

        inMemoryRepository.update(UPDATED_USER)
        val result = inMemoryRepository.retrieve(NICKNAME)

        assertEquals(UPDATED_USER, result)
    }
}