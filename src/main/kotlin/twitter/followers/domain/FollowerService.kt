package twitter.followers.domain

import twitter.followers.domain.errors.UserDoesNotExistError
import twitter.common.domain.UserRepository

interface FollowerServiceDefinition {
    fun followNickname(nickname: String, nicknameToFollow: String): String
    fun retrieveFollowers(nickname: String): List<String>
    fun retrieveFollowing(nickname: String): List<String>
}

class FollowerService(
    private val followerRepository: FollowerRepository,
    private val userRepository: UserRepository,
) : FollowerServiceDefinition {
    override fun followNickname(nickname: String, nicknameToFollow: String): String {
        verifyUsersExist(nickname, nicknameToFollow)
        followerRepository.addFollowerTo(nicknameToFollow, nickname)
        followerRepository.addFollowing(nickname, nicknameToFollow)
        return nicknameToFollow
    }

    override fun retrieveFollowers(nickname: String): List<String> {
        return followerRepository.retrieveFollowers(nickname)
    }

    override fun retrieveFollowing(nickname: String): List<String> {
        return followerRepository.retrieveFollowing(nickname)
    }

    private fun verifyUsersExist(nickname: String, nicknameToFollow: String) {
        if (!userRepository.exists(nickname) || !userRepository.exists(nicknameToFollow))
            throw UserDoesNotExistError()
    }
}
