package twitter.followers.domain

interface FollowerRepository {
    fun addFollowerTo(nickname: String, followerNickname: String)
    fun retrieveFollowers(nickname: String): List<String>
    fun retrieveFollowing(nickname: String): List<String>
    fun addFollowing(nickname: String, followedNickname: String)
}
