package twitter.followers.domain.errors

class UserDoesNotExistError : Throwable()