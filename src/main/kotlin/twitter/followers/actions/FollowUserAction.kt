package twitter.followers.actions

import twitter.followers.domain.FollowerServiceDefinition
import twitter.followers.domain.errors.UserDoesNotExistError

class FollowUserAction(private val followerService: FollowerServiceDefinition) {
    fun execute(nickname: String, nicknameToFollow: String): String? {
        return try {
            followerService.followNickname(nickname, nicknameToFollow)
        } catch (e: UserDoesNotExistError) {
            null
        }
    }
}