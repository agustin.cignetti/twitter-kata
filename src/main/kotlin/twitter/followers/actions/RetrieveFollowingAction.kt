package twitter.followers.actions

import twitter.followers.domain.FollowerServiceDefinition

class RetrieveFollowingAction(private val followerService: FollowerServiceDefinition) {
    fun execute(nickname: String): List<String> {
        return followerService.retrieveFollowing(nickname)
    }
}