package twitter.followers.actions

import twitter.followers.domain.FollowerServiceDefinition

class RetrieveFollowersAction(private val followerService: FollowerServiceDefinition) {
    fun execute(nickname: String): List<String> {
        return followerService.retrieveFollowers(nickname)
    }
}