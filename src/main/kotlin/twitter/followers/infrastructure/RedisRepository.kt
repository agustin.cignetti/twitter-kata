package twitter.followers.infrastructure

import redis.clients.jedis.Jedis
import twitter.followers.domain.FollowerRepository

class RedisRepository (
    host: String = "localhost",
    port: Int = 6379,
) : Jedis(host, port), FollowerRepository {

    private val followerPrefix = "followers-"
    private val followingPrefix = "following-"

    override fun addFollowerTo(nickname: String, followerNickname: String) {
        lpush(followerPrefix + nickname, followerNickname)
    }

    override fun retrieveFollowers(nickname: String): List<String> {
        return lrange(followerPrefix + nickname, 0, -1)
    }

    override fun retrieveFollowing(nickname: String): List<String> {
        return lrange(followingPrefix + nickname, 0, -1)
    }

    override fun addFollowing(nickname: String, followedNickname: String) {
        lpush(followingPrefix + nickname, followedNickname)
    }
}
