package twitter.followers.infrastructure

import twitter.followers.domain.FollowerRepository

class InMemoryRepository : FollowerRepository {

    private val following: MutableMap<String, MutableList<String>> = mutableMapOf()
    private val followersOf: MutableMap<String, MutableList<String>> = mutableMapOf()

    override fun addFollowerTo(nickname: String, followerNickname: String) {
        if (followersOf[nickname] == null)
            followersOf[nickname] = mutableListOf()
        followersOf[nickname]?.add(followerNickname)
    }

    override fun retrieveFollowers(nickname: String): List<String> {
        if (followersOf[nickname] == null)
            return emptyList()
        return followersOf[nickname]!!
    }

    override fun retrieveFollowing(nickname: String): List<String> {
        if (following[nickname] == null)
            return emptyList()
        return following[nickname]!!
    }

    override fun addFollowing(nickname: String, followedNickname: String) {
        if (following[nickname] == null)
            following[nickname] = mutableListOf()
        following[nickname]?.add(followedNickname)
    }
}