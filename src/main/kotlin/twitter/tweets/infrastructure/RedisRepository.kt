package twitter.tweets.infrastructure

import redis.clients.jedis.Jedis
import twitter.tweets.domain.TweetRepository

class RedisRepository (
    host: String = "localhost",
    port: Int = 6379,
) : Jedis(host, port), TweetRepository {

    private val prefix = "tweets-"

    override fun add(tweet: String, nickname: String) {
        lpush(prefix + nickname, tweet)
    }

    override fun retrieve(nickname: String): List<String> {
        return lrange(prefix + nickname, 0, -1)
    }
}