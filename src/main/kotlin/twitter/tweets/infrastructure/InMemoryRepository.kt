package twitter.tweets.infrastructure

import twitter.tweets.domain.TweetRepository

class InMemoryRepository : TweetRepository {

    private val userTweets: MutableMap<String, MutableList<String>> = mutableMapOf()

    override fun add(tweet: String, nickname: String) {
        if (userTweets[nickname] == null)
            userTweets[nickname] = mutableListOf()
        userTweets[nickname]?.add(tweet)
    }

    override fun retrieve(nickname: String): List<String> {
        if (userTweets[nickname] == null)
            return emptyList()
        return userTweets[nickname]!!
    }
}