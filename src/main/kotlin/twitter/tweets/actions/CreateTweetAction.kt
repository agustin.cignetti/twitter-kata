package twitter.tweets.actions

import twitter.tweets.domain.TweetServiceDefinition
import twitter.tweets.domain.errors.UserDoesNotExistError

class CreateTweetAction(private val tweetService: TweetServiceDefinition) {
    fun execute(tweet: String, nickname: String): String? {
        return try {
            tweetService.tweet(tweet, nickname)
        } catch (e: UserDoesNotExistError) {
            null
        }
    }
}