package twitter.tweets.actions

import twitter.tweets.domain.TweetServiceDefinition

class ReadTweetsAction(private val tweetService: TweetServiceDefinition) {
    fun execute(nickname: String): List<String> {
        return tweetService.retrieveTweets(nickname)
    }
}