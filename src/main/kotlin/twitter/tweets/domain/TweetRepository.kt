package twitter.tweets.domain

interface TweetRepository {
    fun add(tweet: String, nickname: String)
    fun retrieve(nickname: String): List<String>
}
