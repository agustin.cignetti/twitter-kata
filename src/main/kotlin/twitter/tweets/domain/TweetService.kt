package twitter.tweets.domain

import twitter.common.domain.UserRepository
import twitter.tweets.domain.errors.UserDoesNotExistError

interface TweetServiceDefinition {
    fun tweet(tweet: String, nickname: String): String
    fun retrieveTweets(nickname: String): List<String>
}

class TweetService(
    private val tweetRepository: TweetRepository,
    private val userRepository: UserRepository,
) : TweetServiceDefinition {

    override fun tweet(tweet: String, nickname: String): String {
        verifyUserExists(nickname)
        tweetRepository.add(tweet, nickname)
        return tweet
    }

    override fun retrieveTweets(nickname: String): List<String> {
        return tweetRepository.retrieve(nickname)
    }

    private fun verifyUserExists(nickname: String) {
        if (!userRepository.exists(nickname))
            throw UserDoesNotExistError()
    }
}
