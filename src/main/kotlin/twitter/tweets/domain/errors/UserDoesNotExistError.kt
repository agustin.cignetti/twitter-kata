package twitter.tweets.domain.errors

class UserDoesNotExistError : Throwable()
