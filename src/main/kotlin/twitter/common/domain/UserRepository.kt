package twitter.common.domain

import twitter.users.domain.models.User

interface UserRepository {
   fun add(user: User)
   fun exists(nickname: String): Boolean
   fun retrieve(nickname: String): User
   fun update(user: User)
}