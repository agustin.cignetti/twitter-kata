package twitter.common.infrastructure

import redis.clients.jedis.Jedis
import twitter.common.domain.UserRepository
import twitter.users.domain.models.User

class RedisRepository(
    host: String = "localhost",
    port: Int = 6379,
) : Jedis(host, port), UserRepository {

    override fun add(user: User) {
        hset(user.nickname, mapOf("fullName" to user.fullName, "password" to user.password))
    }

    override fun exists(nickname: String): Boolean {
        return hexists(nickname, "fullName")
    }

    override fun retrieve(nickname: String): User {
        val userData = hgetAll(nickname)
        return User(nickname, userData["fullName"]!!, userData["password"]!!)
    }

    override fun update(user: User) {
        hset(user.nickname, mapOf("fullName" to user.fullName, "password" to user.password))
    }
}
