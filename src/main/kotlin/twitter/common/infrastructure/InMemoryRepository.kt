package twitter.common.infrastructure

import twitter.common.domain.UserRepository
import twitter.users.domain.models.User

class InMemoryRepository : UserRepository {
    private val userList: MutableList<User> = mutableListOf()

    override fun add(user: User) {
        userList.add(user)
    }

    override fun exists(nickname: String): Boolean {
        return userList.any { it.nickname == nickname }
    }

    override fun retrieve(nickname: String): User {
        return userList.first { it.nickname == nickname }
    }

    override fun update(user: User) {
        userList.removeIf { it.nickname == user.nickname}
        userList.add(user)
    }
}