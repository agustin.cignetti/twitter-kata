package twitter

import twitter.users.actions.CreateUserAction
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import io.javalin.http.Context
import twitter.common.domain.UserRepository
import twitter.common.infrastructure.RedisRepository as InMemoryUserRepository
import twitter.followers.actions.FollowUserAction
import twitter.followers.actions.RetrieveFollowersAction
import twitter.followers.actions.RetrieveFollowingAction
import twitter.followers.domain.FollowerRepository
import twitter.followers.domain.FollowerService
import twitter.tweets.actions.CreateTweetAction
import twitter.tweets.actions.ReadTweetsAction
import twitter.tweets.domain.TweetRepository
import twitter.tweets.domain.TweetService
import twitter.tweets.infrastructure.RedisRepository as InMemoryTweetRepository
import twitter.followers.infrastructure.RedisRepository as InMemoryFollowerRepository
import twitter.users.actions.UpdateUserAction
import twitter.users.domain.UserService

class Application {

    private var retrieveFollowingAction: RetrieveFollowingAction
    private var readTweetsAction: ReadTweetsAction
    private var createTweetAction: CreateTweetAction
    private var tweetService: TweetService
    private var tweetRepository: TweetRepository
    private var retrieveFollowersAction: RetrieveFollowersAction
    private var followUserAction: FollowUserAction
    private var updateUserAction: UpdateUserAction
    private var followerService: FollowerService
    private var followerRepository: FollowerRepository
    private var userRepository: UserRepository
    private var userService: UserService
    private var createUserAction: CreateUserAction

    init {
        userRepository = InMemoryUserRepository()
        followerRepository = InMemoryFollowerRepository()
        tweetRepository = InMemoryTweetRepository()

        userService = UserService(userRepository)
        followerService = FollowerService(followerRepository, userRepository)
        tweetService = TweetService(tweetRepository, userRepository)

        createUserAction = CreateUserAction(userService)
        updateUserAction = UpdateUserAction(userService)
        followUserAction = FollowUserAction(followerService)
        retrieveFollowersAction = RetrieveFollowersAction(followerService)
        createTweetAction = CreateTweetAction(tweetService)
        readTweetsAction = ReadTweetsAction(tweetService)
        retrieveFollowingAction = RetrieveFollowingAction(followerService)
    }

    private val app: Javalin = Javalin.create().routes {
        post("/users") { runCreateUserAction(it) }
        put("/users/:nickname") { runUpdateUserAction(it) }
        put("/followers/:nickname") { runFollowUserAction(it) }
        get("/followers/:nickname") { runRetrieveFollowersAction(it).let { result -> it.json(result) } }
        post("/tweets/:nickname") { runCreateTweetAction(it) }
        get("/tweets/:nickname") { runReadTweetsAction(it).let { result -> it.json(result)} }
        get("/following/:nickname") { runRetrieveFollowingAction(it).let { result -> it.json(result) } }
    }

    fun start(port: Int) {
        app.start(port)
    }

    fun stop() {
        app.stop()
    }

    private fun runCreateUserAction(ctx: Context): Context {
        val result = createUserAction.execute(
            ctx.formParam("nickname")!!,
            ctx.formParam("fullName")!!,
            ctx.formParam("password")!!,
        )
        return makeOkOrErrorResponse(result, ctx, "Nickname Already Taken")
    }

    private fun runUpdateUserAction(ctx: Context): Context {
        val result = updateUserAction.execute(ctx.pathParam("nickname"), ctx.formParam("fullName")!!)
        return makeOkOrErrorResponse(result, ctx, "User Does Not Exist")
    }

    private fun runRetrieveFollowersAction(ctx: Context) : List<String> {
        return retrieveFollowersAction.execute(ctx.pathParam("nickname"))
    }

    private fun runFollowUserAction(ctx: Context): Context {
        val result = followUserAction.execute(ctx.formParam("nickname")!!, ctx.pathParam("nickname"))
        return makeOkOrErrorResponse(result, ctx, "User Does Not Exist")
    }

    private fun runCreateTweetAction(ctx: Context): Context {
        val result = createTweetAction.execute(ctx.formParam("tweet")!!, ctx.pathParam("nickname"))
        return makeOkOrErrorResponse(result, ctx, "User Does Not Exist")
    }

    private fun runReadTweetsAction(ctx: Context): List<String> {
        return readTweetsAction.execute(ctx.pathParam("nickname"))
    }

    private fun runRetrieveFollowingAction(ctx: Context): List<String> {
        return retrieveFollowingAction.execute(ctx.pathParam("nickname"))
    }

    private fun makeOkOrErrorResponse(result: Any?, ctx: Context, errorMessage: String): Context {
        if (result != null) {
            ctx.status(200).json(result)
        } else {
            ctx.status(403).contentType("application/json")
                    .result("""{"msg": "$errorMessage"}""")
        }
        return ctx
    }
}