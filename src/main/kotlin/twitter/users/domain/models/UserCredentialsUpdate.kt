package twitter.users.domain.models

data class UserCredentialsUpdate(val nickname: String, val fullName: String)
