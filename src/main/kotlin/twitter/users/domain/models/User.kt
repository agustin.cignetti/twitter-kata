package twitter.users.domain.models

data class User(val nickname: String, val fullName: String, val password: String)
