package twitter.users.domain.models

data class UserCredentials(val nickname: String, val fullName: String, val password: String)
