package twitter.users.domain

import twitter.common.domain.UserRepository
import twitter.users.domain.errors.NickNameAlreadyTakenError
import twitter.users.domain.errors.UserDoesNotExistError
import twitter.users.domain.models.UserCredentialsUpdate
import twitter.users.domain.models.User
import twitter.users.domain.models.UserCredentials

interface UserServiceDefinition {
    fun createUser(userCredentials: UserCredentials) : User
    fun updateUser(userCredentials: UserCredentialsUpdate) : User
}

class UserService(private val userRepository: UserRepository) : UserServiceDefinition {
    override fun createUser(userCredentials: UserCredentials) : User {
        verifyDuplicateNickname(userCredentials)
        val user = getUserFrom(userCredentials)
        userRepository.add(user)
        return user
    }

    override fun updateUser(userCredentials: UserCredentialsUpdate): User {
        verifyUserExists(userCredentials)
        val updatedUser = updatedUserFrom(userCredentials)
        userRepository.update(updatedUser)
        return updatedUser
    }

    private fun updatedUserFrom(userCredentials: UserCredentialsUpdate): User {
        val user = userRepository.retrieve(userCredentials.nickname)
        return User(user.nickname, userCredentials.fullName, user.password)
    }

    private fun getUserFrom(userCredentials: UserCredentials): User {
        return User(userCredentials.nickname, userCredentials.fullName, userCredentials.password)
    }

    private fun verifyUserExists(userCredentials: UserCredentialsUpdate) {
        if (!userRepository.exists(userCredentials.nickname)) {
            throw UserDoesNotExistError()
        }
    }

    private fun verifyDuplicateNickname(userCredentials: UserCredentials) {
        if (userRepository.exists(userCredentials.nickname)) {
            throw NickNameAlreadyTakenError()
        }
    }
}
