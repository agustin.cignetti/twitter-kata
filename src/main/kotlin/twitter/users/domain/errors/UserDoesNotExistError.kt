package twitter.users.domain.errors

class UserDoesNotExistError : Throwable()