package twitter.users.domain.errors

class NickNameAlreadyTakenError : Throwable()
