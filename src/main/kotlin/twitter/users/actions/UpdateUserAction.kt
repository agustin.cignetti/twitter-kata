package twitter.users.actions

import twitter.users.domain.UserServiceDefinition
import twitter.users.domain.errors.NickNameAlreadyTakenError
import twitter.users.domain.errors.UserDoesNotExistError
import twitter.users.domain.models.User
import twitter.users.domain.models.UserCredentialsUpdate

class UpdateUserAction(private val userService: UserServiceDefinition) {
    fun execute(nickname: String, fullName: String): User? {
        val userCredentials = UserCredentialsUpdate(nickname, fullName)
        return try {
            userService.updateUser(userCredentials)
        } catch (e: UserDoesNotExistError) {
            null
        }
    }
}
