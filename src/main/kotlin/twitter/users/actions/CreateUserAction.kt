package twitter.users.actions

import twitter.users.domain.errors.NickNameAlreadyTakenError
import twitter.users.domain.models.User
import twitter.users.domain.models.UserCredentials
import twitter.users.domain.UserServiceDefinition

class CreateUserAction(private val userService: UserServiceDefinition) {
    fun execute(nickname: String, fullName: String, password:String): User? {
        val userCredentials = UserCredentials(nickname, fullName, password)
        return try {
            userService.createUser(userCredentials)
        } catch (e: NickNameAlreadyTakenError) {
            null
        }
    }
}
